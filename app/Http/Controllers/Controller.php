<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * 
     *
     * @param [type] $request
     * @param integer $action (1: create, 2: update, 3:delete)
     * @param boolean $is_success
     * @return void
     */
    public function alertMessage($request ,$action = 1,$is_success = true, $error_message = '')
    {
        # code...
        if($is_success == true){
            if($action == 1){
                $request->session()->flash('success',"Created record successfully!".$error_message);
            }
            else if($action == 2){
                $request->session()->flash('success',"Updated record successfully! ".$error_message);
            }
            else{
                $request->session()->flash('success',"Deleted record successfully! ".$error_message);

            }
        }
        else{
            if($action == 1){
                $request->session()->flash('error',"Creating record failed! ".$error_message);
            }
            else if($action == 2){
                $request->session()->flash('error',"Updating record failed! ".$error_message);
            }
            else{
                $request->session()->flash('error',"Deleting record failed! ".$error_message);
            }
        }
        
    }
}
