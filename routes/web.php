<?php

use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::group(['middleware' => 'auth'], function(){
    Route::get('/', function () {
        return redirect('user');
    });
    
    Route::resource('user', UserController::class);
    Route::get('user/password/{id}', [UserController::class,'password'])->name('user.password');
    Route::put('user/change-password/{id}', [UserController::class,'changePassword'])->name('user.change.password');
    Route::get('/home', function ()
    {
        return redirect('user');
    })->name('home');
});