@extends('layouts.master')
@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

      <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h5 mb-0 text-gray-800"> User Details </h1>
        <div>
          <a href="{{route('user.edit',@$data->id)}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-add"></i> Update</a>
          <a href="{{route('user.index')}}" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm"><i class="fas fa-add"></i> Back</a>

        </div>
      </div>
      <!-- DataTales Example -->
      <div class="card shadow mb-4">
        {{-- <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Create Case</h6>
        </div> --}}
        <div class="card-body">
          <div class="container" style="padding-top:10px;padding-bottom:10px">
            <div class="form-group row" >
              <label class="control-label col-sm-2 " for="query_number" ><b>Name</b></label>
              <div class="col-sm-10">
                <p>{{$data->name}}</p>
              </div>
            </div>
            <div class="form-group row" >
              <label class="control-label col-sm-2 " for="query_number" ><b>Phone</b></label>
              <div class="col-sm-10">
                <p>{{$data->email}}</p>
              </div>
            </div>
            <div class="form-group row" >
              <label class="control-label col-sm-2 " for="query_number" ><b>Created By</b></label>
              <div class="col-sm-10">
                <p>{{@$creator->name??'N/A'}}</p>
              </div>
            </div>
            <div class="form-group row" >
              <label class="control-label col-sm-2 " for="query_number" ><b>Updated By</b></label>
              <div class="col-sm-10">
                <p>{{@$updater->name??'N/A'}}</p>
              </div>
            </div>
            <div class="form-group row" >
              <label class="control-label col-sm-2 " for="query_number" ><b>Created At</b></label>
              <div class="col-sm-10">
                <p>{{$data->created_at}}</p>
              </div>
            </div>
            <div class="form-group row" >
              <label class="control-label col-sm-2 " for="query_number" ><b>Updated At</b></label>
              <div class="col-sm-10">
                <p>{{$data->updated_at}}</p>
              </div>
            </div>
          </div>
        </div>
      </div>

      </div>
      <!-- /.container-fluid -->

@endsection
