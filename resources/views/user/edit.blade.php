@extends('layouts.master')
@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

      <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h5 mb-0 text-gray-800">Update User</h1>
        <a href="{{route('user.index')}}" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm"><i class="fas fa-add"></i> Back</a>
      </div>
      <!-- DataTales Example -->
      <div class="card shadow mb-4">
        {{-- <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Create Case</h6>
        </div> --}}
        <div class="card-body">
          <div class="container" style="padding-top:10px;padding-bottom:10px">
            <div class=" col-md-12">
              <form class="form-horizontal" method="POST" action="{{route('user.update',$data->id)}}">
                @csrf
                @method('PUT')
                <div class="form-group row" >
                  <label class="control-label col-sm-2 form-label-text text-md-right text-lg-right text-xl-right" for="query_number">Name</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" value="{{old('name')??$data->name}}" id="name" placeholder="Enter Name" name="name">
                    <span class="text-danger">{{ $errors->first('name') }}</span>
                  </div>
                </div>
                <div class="form-group row" >
                  <label class="control-label col-sm-2 form-label-text text-md-right text-lg-right text-xl-right" for="query_number">Email</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" value="{{old('email')??$data->email}}" id="email" placeholder="Enter Email" name="email">
                    <span class="text-danger">{{ $errors->first('email') }}</span>
                  </div>
                </div>
                {{-- <div class="form-group row" >
                  <label class="control-label col-sm-2 form-label-text text-md-right text-lg-right text-xl-right" for="query_number">Password</label>
                  <div class="col-sm-10">
                    <input type="password" class="form-control" value="{{old('password')??$data->password}}"  id="password" placeholder="Enter Password" name="password">
                    <span class="text-danger">{{ $errors->first('password') }}</span>
                  </div>
                </div>
                <div class="form-group row" >
                  <label class="control-label col-sm-2 form-label-text text-md-right text-lg-right text-xl-right" for="query_number">Password Confirmation</label>
                  <div class="col-sm-10">
                    <input type="password" class="form-control"  value="{{old('password_confirmation')??$data->password}}" id="password_confirmation" placeholder="Enter Password Confirmation" name="password_confirmation">
                    <span class="text-danger">{{ $errors->first('password_confirmation') }}</span>
                  </div>
                </div> --}}
                <div class="form-group " style="text-align:end; padding-top:10px">
                    
                  <button type="submit" class="btn btn-primary btn-sm pull-right" > Save </button>
                </div>
              </form>
            </div> 
          </div>
        </div>
      </div>

      </div>
      <!-- /.container-fluid -->

@endsection