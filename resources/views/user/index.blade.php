@extends('layouts.master')
@section('style')
  @include('partials.css-plugin-for-table')

@endsection
@section('content')
    <!-- Begin Page Content -->
    
    <div class="container-fluid">
        
        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
          <h1 class="h5 mb-0 text-gray-800">User List</h1>
          <div >

            <a href="{{route('user.create')}}" class="btn btn-sm btn-primary "><i class="fas fa-add"></i> Create New</a>

          </div>
        </div>
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
          {{-- <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
          </div> --}}
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <td >&numero;</td>
                    <th style="width:12%">Name</th>
                    <th>Email</th>
                    <th style="width:185px">Created At</th>
                    <th style="width:185px">Updated At</th>
                    <th style="text-align:center; width:100px">Actions</th>

                  </tr>
                </thead>
                
                <tbody>
                  @php
                      $page = 0;
                      if(Request::get('page'))
                        $page  = Request::get('page')-1;
                      
                      $index = $page*20 + 1;
                  @endphp
                  @foreach ($datas as $key => $item)
                    <tr>
                      <td>{{$key + $index}}</td>
                      <td>{{$item->name}}</td>
                      <td>{{$item->email}}</td>
                      <td>{{$item->created_at}}</td>
                      <td>{{$item->updated_at}}</td>
                      <td align="center">
                        <a href="{{route('user.edit',$item->id)}}">
                          <i class="fas fa-edit"></i>
                        </a>
                        <a href="#" data-toggle="modal" data_id="{{$item->id}}" data-target="#modal-delete" onclick="onClickDelete('user/destroy',{{$item->id}})">
                          <i class="fas fa-trash"></i>
                        </a>
                        <a href="{{route('user.show',$item->id)}}">
                          <i class="fas fa-eye"></i>
                        </a>
                        <a href="{{route('user.password',$item->id)}}">
                          <i class="fas fa-key"></i>
                        </a>
                      </td>

                    </tr>
                  @endforeach
                  
                </tbody>
              </table>
            </div>
            {{-- {{$datas->appends(request()->except('page'))->links()}} --}}
          </div>
        </div>

    </div>
      <!-- /.container-fluid -->
@endsection

@section('script')
    @include('partials.js-plugin-for-table')

@endsection