@extends('layouts.auth')

@section('title')
    <title>UM | register</title>
@endsection

@section('content')
    <!-- Outer Row -->
    <div class="row justify-content-center">

        <div class="col-xl-6 col-lg-9 col-md-11">
        
            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        {{-- <div class="col-lg-5 d-none d-lg-block bg-register-image"></div> --}}
                        <div class="col-lg-12">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
                                </div>
                                <form method="POST" action="{{ route('register') }}">
                                    @csrf
                                    <div class="form-group">

                                        <input type="text" class="form-control form-control-user" id="Name"
                                            placeholder="Name" name="name" value="{{(old('name'))}}">
                                        <span class="text-danger">{{ $errors->first('name') }}</span>    

                                    </div>
                                    <div class="form-group">
                                        <input type="email" class="form-control form-control-user" id="exampleInputEmail"
                                            placeholder="Email Address" name="email" value="{{(old('email'))}}">
                                        <span class="text-danger">{{ $errors->first('email') }}</span>    

                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-6 mb-3 mb-sm-0">
                                            <input type="password" class="form-control form-control-user"
                                                id="password" placeholder="Password" name="password" >
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="password" class="form-control form-control-user"
                                                id="password-confirmation" placeholder="Password Confirmation" name="password_confirmation">
                                        </div>
                                        <div class="col-sm-12">
                                            <span class="text-danger">{{ $errors->first('password') }}</span>    
                                        </div>

                                    </div>
                                    
                                    <button type="submit" class="btn btn-primary btn-user btn-block">Register Account</button>

                                    {{-- <hr>
                                    <a href="index.html" class="btn btn-google btn-user btn-block">
                                        <i class="fab fa-google fa-fw"></i> Register with Google
                                    </a>
                                    <a href="index.html" class="btn btn-facebook btn-user btn-block">
                                        <i class="fab fa-facebook-f fa-fw"></i> Register with Facebook
                                    </a> --}}
                                </form>
                                <hr>
                                {{-- <div class="text-center">
                                    <a class="small" href="forgot-password.html">Forgot Password?</a>
                                </div> --}}
                                <div class="text-center">
                                    <a class="small" href="{{url('login')}}">Already have an account? Login!</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
@endsection
