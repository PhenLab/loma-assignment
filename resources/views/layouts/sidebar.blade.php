<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

  <!-- Sidebar - Brand -->
  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{url('/')}}">
    {{-- <div class="sidebar-brand-icon rotate-n-15">
      <i class="fas fa-laugh-wink"></i>
    </div> --}}
    <div class="sidebar-brand-text mx-3">User Management
      {{-- <sup>2</sup> --}}
    </div>
  </a>

  <!-- Divider -->
  <hr class="sidebar-divider my-0">

  
  <!-- Nav Item - Dashboard -->
  {{-- <li class="nav-item {{App\Helper\Helper::getActiveMenu('dashboard')}}">
    <a class="nav-link" href="{{url('dashboard')}}">
      <i class="fas fa-fw fa-tachometer-alt"></i>
      <span>Dashboard</span></a>
  </li> --}}

  <?php
      $case = array(
          'user.index',
          'user.create',
          'user.edit',
          'user.show',
      );
  ?>
  <!-- Nav Item - Dashboard -->
  <li class="nav-item {{App\Helper\Helper::getActiveMenu($case)}}">
    <a class="nav-link" href="{{route('user.index')}}">
      <i class="fas fa-fw fa-tachometer-alt"></i>
      <span>User</span></a>
  </li>



</ul>
<!-- End of Sidebar -->